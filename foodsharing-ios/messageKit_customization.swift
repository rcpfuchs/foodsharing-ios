//
//  messageKit_customization.swift
//  foodsharing-ios
//
//  Created by Baudisgroup User on 28.04.19.
//

import Foundation
import MessageInputBar

class CustomInputBar: MessageInputBar {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure() {
        backgroundView.backgroundColor = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
        sendButton.title = "Senden"
        sendButton.setTitleColor(custom_color.leaf, for: .normal)
        sendButton.setSize(CGSize(width: 64, height: 36), animated: false)
        setRightStackViewWidthConstant(to: 64, animated: false)
        inputTextView.backgroundColor = .white
        inputTextView.placeholderLabel.text = "Nachricht..."
        inputTextView.tintColor = custom_color.leaf
        inputTextView.placeholderTextColor = UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)
        inputTextView.textContainerInset = UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16)
        inputTextView.placeholderLabelInsets = UIEdgeInsets(top: 8, left: 20, bottom: 8, right: 20)
        inputTextView.layer.cornerRadius = 4.0
        inputTextView.layer.masksToBounds = true
        inputTextView.scrollIndicatorInsets = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
//        setLeftStackViewWidthConstant(to: 36, animated: false)
//        setStackViewItems([button], forStack: .left, animated: false)
        
    }
    
}
