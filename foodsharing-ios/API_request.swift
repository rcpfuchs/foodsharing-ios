//
//  File.swift
//  LoginScreenSwift
//
//  Created by Baudisgroup User on 07.04.19.
//  Copyright © 2019 com.kaleidosstudio. All rights reserved.
//


import Foundation

func fetch_token (user: String, psw: String, completion: @escaping (_ error: loginError?) -> ()) {

    let session = URLSession.shared
    let url = URL(string: Constants.base_url + "/api/user/login")

    let request = NSMutableURLRequest(url: url!)
    request.httpMethod = "POST"
    let paramToSend = "email=" + user + "&password=" + psw
    request.httpBody = paramToSend.data(using: String.Encoding.utf8)

    let task = session.dataTask(with: request as URLRequest, completionHandler: {
        (data, response, error) in

        if (error as NSError?) != nil {

            //            if error.code == -1004 {
            //                completion(.noConnection)
            //            }
            completion(.noConnection)
            return
        }


        if let res = response as? HTTPURLResponse{

            if res.statusCode == 401 {
                completion(.wrongPassword)
                return
            }

            let header = res.allHeaderFields

            if let cookie = header[AnyHashable("Set-Cookie")] {

                if let cookieStr = cookie as? String {

                    var CSRF_token = cookieStr.split(separator: ";") [0]
                    CSRF_token = CSRF_token.split(separator: "=") [1]
                    UserDefaults.standard.set(String(CSRF_token), forKey: "token")
                    UserDefaults.standard.set(true, forKey: "userlogin")


                    print("finish assigning token")
                    completion(nil)
                }
            }

        }

        guard let _:Data = data else
        {
            return
        }

        let json:Any?

        do
        {
            json = try JSONSerialization.jsonObject(with: data!, options: [])
            if let json = json as! [String:Any]? {
                UserDefaults.standard.set(json["id"], forKey: "id")
                UserDefaults.standard.set(json["name"] as! String, forKey: "name")
            }
        }
        catch
        {
            return
        }

    })
    task.resume()
}



func fetch_API_response(query_API: API_call, API_id: Int, method: String, headers: [[String:String]]?, response_type: String, completion: @escaping (_ json: Any?, _ error: Error?) -> ()) {

    let session = URLSession(configuration: .default)

    let token = UserDefaults.standard.string(forKey:"token")!

    let query = query_API.API_endpoint(API_id)

    let url = URL(string: Constants.base_url + query)

    let request = NSMutableURLRequest(url: url!)
    request.addValue(token, forHTTPHeaderField: "X-CSRF-Token")

    if let headers = headers {
        for headerDict in headers {
            for (key, value) in headerDict {
                request.addValue(value, forHTTPHeaderField: key)
            }
        }
    }

    request.httpMethod = method

    let task = session.dataTask(with: request as URLRequest, completionHandler: {
        (data, response, error) in


        guard let _:Data = data else
        {
            return
        }

        let json:Any?

        do
        {
            json = try JSONSerialization.jsonObject(with: data!, options: [])
        }
        catch
        {
//            print( data!)
            return
        }

        if response_type == "dict"{
            guard let server_response = json as? NSDictionary else
            {
                completion(nil, error)
                return
            }
            completion(server_response, error)
        }
        else if response_type == "array" {
            guard let data_array = json as? NSArray else
            {
                completion(nil, error)
                return
            }

            let array_response = data_array as NSArray as! [NSDictionary]
            completion(array_response, error)
        }


    })

    task.resume()
}


func post_API(query_API: API_call, API_id: Int, method: String, headers: [[String:String]]?) {

    let session = URLSession(configuration: .default)

    let token = UserDefaults.standard.string(forKey:"token")!

    let query = query_API.API_endpoint(API_id)

    let url = URL(string: Constants.base_url + query)

    let request = NSMutableURLRequest(url: url!)
    request.addValue(token, forHTTPHeaderField: "X-CSRF-Token")

    var httpBodyString = ""

    if let headers = headers {
        for headerDict in headers {
            for (key, value) in headerDict {
                httpBodyString += "\(key)=\(value)&"
            }
        }
    }

    request.httpBody = httpBodyString.data(using: String.Encoding.utf8)
    request.httpMethod = method

    let task = session.dataTask(with: request as URLRequest, completionHandler: {
        (data, response, error) in
        guard let _:Data = data else
        {
            return
        }

        let json:Any?

        do
        {
            json = try JSONSerialization.jsonObject(with: data!, options: [])
        }
        catch
        {
//            print(String(data: data!, encoding: .utf8))

            return
        }


    })

    task.resume()
}
