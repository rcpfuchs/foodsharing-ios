//
//  identicon.swift
//  foodsharing-ios
//
//  Created by Baudisgroup User on 29.04.19.
//

import Foundation
import UIKit
import CommonCrypto

func sha256(data : Data) -> Data {
    var hash = [UInt8](repeating: 0,  count: Int(CC_SHA256_DIGEST_LENGTH))
    data.withUnsafeBytes {
        _ = CC_SHA256($0, CC_LONG(data.count), &hash)
    }
    return Data(bytes: hash)
}

extension Data {
    private static let pentaAlphabet = "01234".unicodeScalars.map { $0 }
    
    public func pentaEncodedString() -> String {
        return String(self.reduce(into: "".unicodeScalars, { (result, value) in
            
            result.append(Data.pentaAlphabet[Int(value%5)])
        }))
    }
}

func StringHash2penta(_ str: String) -> String {
    
    let abc = sha256(data: Data(str.data(using: .utf8)!))
    return abc.pentaEncodedString()
    
}

func makeOrigins(_ str: String) -> [(Int, Int)] {
    //    let str = abc.pentaEncodedString()
    var i = 0
    var origins: [(Int, Int)] = []
    var set: Set<Int> = Set()
    for char in str.map(String.init) {
        
        let charInt = Int(char)!
        if set.contains(charInt) {
            i += 1
            if i == 3 {
                break
            }
            set = Set()
        } else {

            origins.append((i, charInt))
            if i != 3 {
                origins.append((4 - i, charInt))
            }
            
            set.insert(charInt)
        }
        
    }
    
    return origins
}

struct IconMaker {
    
    static func makeIcon(with color: UIColor, origins: [(Int, Int)]) -> UIImage {
        
        let size = CGSize(width: 100, height: 100)
        
        let origins_scale = origins.map({(a: Int, b: Int) -> (Int, Int) in return (a*20, b*20)})
        
        // Create graphics context:
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        

        color.setFill()
        
        let icon = UIImage()
        
        for (a,b) in origins_scale {
            let rect = CGRect(origin: CGPoint(x: a, y: b), size: CGSize(width: 20, height: 20))
            UIRectFill(rect)
            icon.draw(in: rect, blendMode: .normal, alpha: 1.0)
        }
        
        
        // Create new image and release context:
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
        
    }
}

func makeIdenticon(_ str: String) -> UIImage {
    let sha = sha256(data: Data(str.data(using: .utf8)!))
    let sha_penta = sha.pentaEncodedString()
    let origins = makeOrigins(sha_penta)
    let icon = IconMaker.makeIcon(with: custom_color.leaf, origins: origins)
    return icon
}
