//
//  MainCoordinator.swift
//  foodsharing-ios
//
//  Created by Simon Theiß on 31.05.19.
//

import UIKit

// Coordinator implementation for the initial App View
class AuthCoordinator: ParentCoordinator {
    
    // entry method of the navigation stack.
    // TODO handle user already logged in by presenting the correct UIViewController
    override func start() {
        let vc = LoginViewController.instantiate()
        vc.userDidLogin = {
            [weak self] in
            self?.userDidLogin()
        }
        navigationController.pushViewController(vc, animated: false)
        navigationController.setNavigationBarHidden(true, animated: false)
    }
    
    var tabBarController: MainTabBarController?
    
    // MARK: - Login/Logout
    
    
    // the user did login
    func userDidLogin() {
        let vc = MainTabBarController.instantiate()
        navigationController.present(vc, animated: true, completion: nil)
        self.tabBarController = vc
    }
    
    // the user did logout
    func userDidLogout() {
        // remove user data
        // todo show loginviewcontroller again
    }
    
}
