//
//  Coordinator.swift
//  foodsharing-ios
//
//  Created by Simon Theiß on 31.05.19. Idea from https://www.hackingwithswift.com/articles/71/how-to-use-the-coordinator-pattern-in-ios-apps
//

import UIKit

/**
 Coordinates the transactions from one UIViewController to another UIViewController
 */
protocol Coordinator: class {

    // references to childCoordinators that may be displayed during user interaction
    var childCoordinators: [Coordinator] { get set }
    
    // reference to the UINavigationController that is used to present Views
    var navigationController: UINavigationController { get set }
    
    // 
    func start()
    
}
