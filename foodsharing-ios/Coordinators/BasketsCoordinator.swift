//
//  BasketsCoordinator.swift
//  foodsharing-ios
//
//  Created by Simon Theiß on 31.05.19.
//

import UIKit

// Coordinator implementation for the baskets tab
class BasketsCoordinator: ParentCoordinator {
    override func start() {
        let vc = UIViewController() // TODO Replace with BasketsController
        vc.tabBarItem = UITabBarItem(title: "Meine Körbe".localized(), image: UIImage(named: "basket"), selectedImage:  UIImage(named: "basket"))
        self.navigationController.pushViewController(vc, animated: false)
    }
}
