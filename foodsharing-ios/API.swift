//
//  API.swift
//  foodsharing-ios
//
//  Created by Baudisgroup User on 09.04.19.
//  Copyright © 2019 com.kaleidosstudio. All rights reserved.
//

import Foundation



enum API_call {
    case getBasketsCoordinate, getmyBaskets, getBasketDetail
    case getConversations, getConversationByid
    case postMessage
    
    func API_endpoint(_ id: Int) -> String {
        switch self{
        case .getBasketsCoordinate:
            return "/api/baskets&type=coordinates"
        case .getConversations:
            return "/api/conversations"
        case .getmyBaskets:
            return "/api/baskets?type=mine"
        case .getConversationByid:
            return "/api/conversations/\(id)"
        case .getBasketDetail:
            return "/api/baskets/\(id)"
        case .postMessage:
            return "/xhrapp.php?app=msg&m=sendmsg"
        }
    }
    
}


enum API_put {
    case putBasketPic
    
    func API_endpoint (_ id: Int) -> String {
        switch self{
        case .putBasketPic:
            return "/api/baskets/\(id)/picture"
        }
    }
}
