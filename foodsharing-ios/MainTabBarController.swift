//
//  RootTabBarController.swift
//  foodsharing-ios
//
//  Created by Simon Theiß on 31.05.19.
//

import UIKit

class MainTabBarController: UITabBarController, Storyboarded {
    
    let messageCoordinator = MessageCoordinator()
    let basketsCoordinator = BasketsCoordinator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        messageCoordinator.start()
        basketsCoordinator.start()
        
        self.viewControllers = [messageCoordinator.navigationController, basketsCoordinator.navigationController]
    }

}
