//
//  Extension.swift
//  foodsharing-ios
//
//  Created by Baudisgroup User on 08.04.19.
//  Copyright © 2019 com.kaleidosstudio. All rights reserved.
//

import Foundation
import UIKit
extension UITextField {
    
    func addDefaultBottomBorder(){
        
        self.borderStyle = .none
        self.layer.backgroundColor = nil
        let bottomLine = CALayer()
        bottomLine.frame = CGRect.init(x: 0, y: self.frame.size.height - 0.5, width: self.frame.size.width, height: 0.5)
//        bottomLine.colors = [UIColor.green.cgColor, UIColor.purple.cgColor, UIColor.red.cgColor]
//        bottomLine.startPoint = CGPoint(x: 0, y: self.frame.size.height)
//        bottomLine.endPoint = CGPoint(x: self.frame.size.width, y: self.frame.size.height)
        bottomLine.backgroundColor = UIColor.lightGray.cgColor
        self.layer.insertSublayer(bottomLine, at: 0)
        
        
    }
    func activateBottomBorder() {

//        if case let first_layer as CAGradientLayer = self.layer.sublayers?.first  {
//            first_layer.frame = CGRect.init(x: 0, y: self.frame.size.height - 2, width: self.frame.size.width, height: 2)
//            first_layer.colors = [UIColor.green.cgColor, UIColor.purple.cgColor]
//        }
        self.layer.sublayers?.first?.frame = CGRect.init(x: 0, y: self.frame.size.height - 2, width: self.frame.size.width, height: 2)
        self.layer.sublayers?.first?.backgroundColor = custom_color.leaf.cgColor
//        self.layer.sublayers?.first?.backgroundColor = UIColor.black.cgColor
    }
    
    func deactivateBottomBorder() {
        self.layer.sublayers?.first?.frame = CGRect.init(x: 0, y: self.frame.size.height - 0.5, width: self.frame.size.width, height: 0.5)
        self.layer.sublayers?.first?.backgroundColor = UIColor.lightGray.cgColor
    }
}

class PaddingTextField: UITextField {
    
    let padding = UIEdgeInsets(top: 0.0, left: 12.0, bottom: 0.0, right: 12.0)
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}

extension String {
    
    func localized(withComment comment: String? = nil) -> String {
        return NSLocalizedString(self, comment: comment ?? "")
    }
    
}
