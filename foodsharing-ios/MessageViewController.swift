//
//  MessageViewController.swift
//  LoginScreenSwift
//
//  Created by Baudisgroup User on 07.04.19.
//  Copyright © 2019 com.kaleidosstudio. All rights reserved.
//

import UIKit

@available(iOS 10.0, *)
class MessageViewController: UITableViewController, Storyboarded {
    
    // MARK: - Properties
    
    var showConversation:((_ conversation:Conversation)->())?
    
    
    // coordinator - manages view controller changes
    weak var coordinator: MessageCoordinator?
    
    var conversations: [Conversation] = [] {
        didSet {
            DispatchQueue.main.async {

                print("conversations cell reloading")
                self.tableView.reloadData()
            }
        }
    }


    override func viewDidLoad() {
        LoadConversations(afterRefresh: false)
        super.viewDidLoad()
        configureRefreshControl()


    }

}

// MARK: - UITableViewDataSource
extension MessageViewController {

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return conversations.count
    }

    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageCell", for: indexPath) as! MessageTableCell

        let conversation = conversations[indexPath.row]
        cell.conversation = conversation
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.showConversation?(conversations[indexPath.row])

        //handle your code here
//        self.performSegue(withIdentifier: "ShowConversationDetail", sender: indexPath)
    }

    //MARK: Actions
//    @IBAction func unwindToMessageList(sender: UIStoryboardSegue) {
//        if let sourceViewController = sender.source as? ChatViewController {
//
//        }
//    }

    @IBAction func unwindToMessageList(segue: UIStoryboardSegue) {
        self.LoadConversations(afterRefresh: false)
    }



    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowConversationDetail", let indexPath = sender as? IndexPath {
            let chatview = segue.destination as! ChatViewController
            chatview.conversation = conversations[indexPath.row]
//            self.addChild(chatview)
//            self.view.addSubview(chatview.view)
//            chatview.didMove(didMove(toParent: self))
        }
    }


}

extension MessageViewController{
    func LoadConversations (afterRefresh: Bool) {
        var conversations = [Conversation]()
        fetch_API_response(query_API: .getConversations, API_id: 0, method: "GET", headers: nil, response_type: "array", completion: {json, error in
            if let json = json as? [[String:Any]] {

                let line_attributes: [NSAttributedString.Key: Any] = [.kern: 0]
                let name_attributes: [NSAttributedString.Key: Any] = [.font: UIFont.boldSystemFont(ofSize: 13)]

                for i in json {
//                    print (i)
                    let tmp_conversation = Conversation()

//                    let last_fs_id = i["last_foodsaver_id"] as? Int

                    tmp_conversation.id = Int(i["id"] as! String)

                    tmp_conversation.time = i["last_ts"] as? String

                    let line = i["last_message"] as! String

                    let tmp_line = line.htmlAttributedString?.string

                    let last_fs_id = i["last_foodsaver_id"] as! String

                    if let member_detail = i["member"] as? [NSDictionary]
                    {
                        let member = member_detail[0]

                        // this is personal message
                        // identify conversation target
                        if tmp_conversation.sender == nil {
                            let search_other_person = member_detail.filter({$0["id"] as! String! != String(UserDefaults.standard.integer(forKey: ("id")))})
                            if search_other_person.count > 0 {
                                tmp_conversation.sender = search_other_person[0]["name"] as? String
//                                print("found conversation target!\n")

                                if case let image_url as String = search_other_person[0]["photo"] {
                                    tmp_conversation.image_url = Constants.base_url + "/images/130_q_" + image_url
                                } else {
                                    tmp_conversation.image_url = Constants.DEFAULT_USER_PICTURE
                                }


                                // add sender name before message as "Du" or opponent name
                                var tmp_member = NSMutableAttributedString()
                                if Int(last_fs_id) == UserDefaults.standard.integer(forKey: "id"){
                                    tmp_member = NSMutableAttributedString(string: "Du: ", attributes: name_attributes)
                                    tmp_member.append(NSAttributedString(string: tmp_line!, attributes: line_attributes))
                                    tmp_conversation.line = tmp_member
                                } else {
                                    tmp_conversation.line = NSAttributedString(string: tmp_line!, attributes: line_attributes)
                                }

                            }
                        }

                        // this is to print in the message line

                        let search_last = member_detail.filter({$0["id"] as! String == last_fs_id})

                        var member_last: NSDictionary
                        if search_last.count > 0 {
                            member_last = search_last[0]

                        } else {
                            member_last = member
                        }

                        // this is group message
                        if let name = i["name"] as?  String{

                            //replaced sender info
                            tmp_conversation.sender = name
                            var tmp_member = NSMutableAttributedString()
                            if Int(last_fs_id) == UserDefaults.standard.integer(forKey: "id"){
                                tmp_member = NSMutableAttributedString(string: "Du: ", attributes: name_attributes)
                            } else {
                                tmp_member = NSMutableAttributedString(string: member_last["name"]! as! String + ": ", attributes: name_attributes)
                            }

                            tmp_member.append(NSAttributedString(string: tmp_line!, attributes: line_attributes))
                            tmp_conversation.line = tmp_member
                            tmp_conversation.image_url = nil
                        }


                    }

                    conversations.append(tmp_conversation)

                }

                self.conversations = conversations
                print("messages fetched!")

                if afterRefresh {
                    DispatchQueue.main.async {
                        self.tableView.refreshControl?.endRefreshing()
                    }
                }
            }
        })

    }
}


extension MessageViewController {
    func configureRefreshControl () {
        // Add the refresh control to your UIScrollView object.
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = custom_color.leaf
        tableView.refreshControl = refreshControl
        tableView.refreshControl?.addTarget(self, action:
            #selector(refreshMessageData),for: .valueChanged)
    }

    @objc private func refreshMessageData(_ sender: Any) {
        // Fetch Weather Data
        LoadConversations(afterRefresh: true)

    }
}
